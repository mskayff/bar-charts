(function($){	
	
	$.fn.barChart = function(options){

		// Animate ?
		var animate = (options.animate != 'undefined')? options.animate : false;

		var scores = [];
		// ----------------------------------------------------------------------------------------
		// Calculate the bar width percentage based on scores
		// ----------------------------------------------------------------------------------------
		for (var i=0;i < options.graphs.length;i++){
			// A bar graph
			var graph = options.graphs[i];
			scores.push(graph.score);
		}		
		// ----------------------------------------------------------------------------------------
		// Max score
		// ----------------------------------------------------------------------------------------
		var maxScore = scores.reduce(function(a, b) { return Math.max(a, b) });
		// ----------------------------------------------------------------------------------------
		// Check for the order flag
		// If order flag is true, will order from highest to lowest score
		// ----------------------------------------------------------------------------------------
		if(options.order != 'undefined' && options.order){
			options.graphs = options.graphs.sort(function(a,b) {
				if(a.score > b.score)
					return -1;
				else if(a.score < b.score)
					return 1;
				else return 0;
			});
		}

		// ----------------------------------------------------------------------------------------
		// Build every item graph
		// ----------------------------------------------------------------------------------------
		for (var i=0;i < options.graphs.length;i++){
			// A bar graph
			var graph = options.graphs[i];

			// Has custom color?
			var color = (graph.color != 'undefined')? graph.color : false;

			// Wrapper for the line bar
			var $barItem = $('<div class="bar-chart-item">');

			// The line bar label
			var $labelItem = $('<div class="col-xs-12 col-sm-2 bar-chart-label">').html(graph.label);

			// Append the label
			$barItem.append($labelItem);

			// Bar graph wrapper
			var $graphItem = $('<div class="col-xs-12 col-sm-10 bar-chart-graph-container">');

			// The bar graph itself
			var $chartGraph = $('<div class="bar-chart-graph">').width(20);

			// Set custom color?
			if (color){
				$chartGraph.css("backgroundColor", color);
			}

			// Set inner text if set
			if (graph.innerText != "undefined" && graph.innerText != ""){
				$chartGraph.append($('<span class="bar-chart-inner-text">').html(graph.innerText));
			}

			// Append the bar graph to its wrapper
			$graphItem.append($chartGraph);

			// Set outter text
			if (graph.outerText != "undefined" && graph.outerText != ""){
				$graphItem.append($('<span class="bar-chart-outer-text">').html(graph.outerText));
			}	

			// Append the graph
			$barItem.append($graphItem);

			// Finnaly append this bar item to the main plugin wrapper
			$(this).append($barItem);

			$barItem.addClass("clearfix");

			// ----------------------------------------------------------------------------------------
			// Calculcate the width based on max Score
			// ----------------------------------------------------------------------------------------
			var width = Math.ceil(graph.score * 100 / maxScore);

			// Handle animation
			if (animate){
				// If animation set, animate the line bars and fadein any outer text.
				$chartGraph.animate({ width : width + "%"}, 500, "swing", function(){
						$(".bar-chart-outer-text").fadeIn(300);
				});
			}
			else{
				// Otherwise set the graph fixed width
				$chartGraph.width(width + "%");
				$(".bar-chart-outer-text").show();
			}

		}

		return this;
	}

})(jQuery);
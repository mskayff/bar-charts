# A Bars Chart JQuery plugin

## Bootstrap JQuery pluging to draw dynamic bar charts

### Requirements

This pluging requires Jquery and Twitter Bootstrap

### Install

Make sure you have included a stable JQuery version and Bootstrap (v3) in your webpage.

Install js and css files:

`<link rel="stylesheet" href="path/to/bar.charts.css">`

`<script src="path/to/bar.charts.js"></script>`

### The HTML markup

Place a `<div id="some-id-for-bar-charts" class="row">` where you want the charts to display.

Remember Bootstrap's row class requires to be within a `class=container` element. If you currently don't have one you must add one to your html.

### Initialize and Setup "Bar Chart"

You can initialize the Bar Chart after the HTML markup has loaded or preferably within a $(document).ready handler using the plugin syntax:

`$("#some-id-for-bar-charts").barChart(options);`

### The options object

The **options** object builds the chart data.

It's a Javascript Object with the following properties:

{
	animate : boolean - Whether the bars will be animated or not,

	graphs : array - An objects array holding the following objects/elements 
		[
			{
				label : string - A String label for this bar
				score : int - A Score number - Will be converted to % to represent the bar width
				innerText : string - Some text showing inside the bar
				outerText : string - Some text showing next to the bar
				color: string - hex color representation (eg: #23A4B9)
			}
		]
}`




